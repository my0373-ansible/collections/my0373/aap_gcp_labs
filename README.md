# aap_gcp_labs
Set of Ansible roles, playbooks and collections to stand up AAP controller demos in the Google Cloud Platform (GCP).

## Getting started

### What you'll need

1. An account on the Google Cloud Platform
2. A project on the Google Cloud Platform
3. A service account with permissions on the GCP project


### Some things you'll want to tweak

1.  Variables you'll want to configure

